package utils;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
//import org.openqa.selenium.phantomjs.PhantomJSDriver;

public class CoreClass {
	
	public static WebDriver driver;		
	public static String baseUrl = "https://mycutebaby.in/contest/participant/?n=5d00be47b6922";

	@BeforeSuite
	public void setUp() throws Exception {
		
		System.setProperty("webdriver.chrome.driver", "src\\main\\resources\\driver\\chromedriver.exe");
		 driver = new ChromeDriver();
		//driver = new PhantomJSDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.get(baseUrl);
		System.out.println("URL is opened.");
	}
	
	@AfterSuite
	public void tearDown() throws Exception {		
		driver.quit();		
	}
	
	public static By waitForElement(By xpath) {
		System.out.println("waitForElement");
		FluentWait wait = new FluentWait(driver).withTimeout(500, TimeUnit.SECONDS).pollingEvery(3, TimeUnit.SECONDS)
				.ignoring(NoSuchElementException.class);

		wait.until(ExpectedConditions.visibilityOfElementLocated(xpath));
		// visibilityOfElementLocated(By.xpath(xpath)));(instead we can use
		// visibilityOfElementLocated )
		return xpath;
	}

}
